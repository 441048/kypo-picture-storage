# KYPO File Storage
This project represents back-end for storing files in KYPO Portal.

## Authors

Role         | UCO          | Name 
------------ | ------------ | -------------
Lead Developer    |   441048     | Šeda Pavel

## Documentation 
Documentation is done in the Swagger framework. It is possible to reach it on the following page:

```
~/kypo-rest-file-storage/api/v1/swagger-ui.html
```

e.g., on localhost it should be:

```
https://localhost:8080/kypo-rest-file-storage/api/v1/swagger-ui.html
```

NOTE: please note that client for that REST API could be generated using [Swagger codegen] (https://swagger.io/tools/swagger-codegen/). It is crucial to annotate each RestController method properly!


## Property File
After the previous steps you have to create properties file according to format shown in [kypo file storage](kypo-file-storage.properties) and save it. 

## Installing Project and Database Migration
Installing by maven:

```
mvn clean install
```
NOTE: To skip integration tests use -DskipITs 

### Database Migration
Prerequisites running PostgreSQL and created the database named 'file-storage' with schema 'public'.
To migrate database data it is necessary to run these two scripts:

```
$ mvn flyway:migrate -Djdbc.url=jdbc:postgresql://{url to DB}/kypo-file-storage -Djdbc.username={username in DB} -Djdbc.password={password to DB}
```
e.g.:
```
$ mvn flyway:migrate -Djdbc.url=jdbc:postgresql://localhost:5432/kypo-file-storage -Djdbc.username=postgres -Djdbc.password=postgres

```

### Run Project
In Intellij Idea:
1. Click on "**Run**" -> "**Edit configurations**".
2. Choose "**KypoFileStorageApplication**" configuration.
3. Add into "**Program arguments**" --path.to.config.file="{path to your config properties}".
4. Run KypoFileStorageApplication

In command line:
```
mvn spring-boot:run -Dpath.to.config.file={path to properties file from step 2}
```

## Used Technologies
The project was built and tested with these technologies, so if you have any unexpected troubles let us know.

```
Maven         : 3.3.9
Java          : OpenJDK 11
Spring Boot   : 2.1.6.RELEASE
Swagger       : 2.9.2
JdbcTemplate  : 5.3.7.Final
Jackson       : 2.9.7
Tomcat        : 9
PostgreSQL    : 11
```
