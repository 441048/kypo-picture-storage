package cz.muni.ics.kypo.file.storage.api;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * The type Db file response dto.
 *
 * @author Pavel Seda
 */
public class DBFileResponseDto {

    private String uuid;
    private String fileName;
    private String fileType;
    private byte[] fileData;
    private LocalDateTime uploadedTime;

    /**
     * Instantiates a new Db file response dto.
     */
    public DBFileResponseDto() {
    }

    /**
     * Instantiates a new Db file response dto.
     *
     * @param uuid         the uuid
     * @param fileName     the file name
     * @param fileType     the file type
     * @param fileData     the file data
     * @param uploadedTime the uploaded time
     */
    public DBFileResponseDto(String uuid, String fileName, String fileType, byte[] fileData, LocalDateTime uploadedTime) {
        this.uuid = uuid;
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileData = fileData;
        this.uploadedTime = uploadedTime;
    }

    /**
     * Gets uuid.
     *
     * @return the uuid
     */
    @ApiModelProperty(value = "Unique reference id to the file", example = "56e8a7fb-64f4-43fb-af02-fe6b01691c76")
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets uuid.
     *
     * @param uuid the uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Gets file name.
     *
     * @return the file name
     */
    @ApiModelProperty(value = "A name of a given file", example = "KYPO-intro-presentation.pptx")
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets file name.
     *
     * @param fileName the file name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets file type.
     *
     * @return the file type
     */
    @ApiModelProperty(value = "A type of a file", example = "application/vnd.openxmlformats-officedocument.presentationml.presentation")
    public String getFileType() {
        return fileType;
    }

    /**
     * Sets file type.
     *
     * @param fileType the file type
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * Get file data byte [ ].
     *
     * @return the byte [ ]
     */
    @ApiModelProperty(value = "Content of a file as a byte array")
    public byte[] getFileData() {
        return fileData;
    }

    /**
     * Sets file data.
     *
     * @param fileData the file data
     */
    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    /**
     * Gets uploaded time.
     *
     * @return the uploaded time
     */
    @ApiModelProperty(value = "A time when a file was uploaded", example = "2019-07-09 14:50:09.467725")
    public LocalDateTime getUploadedTime() {
        return uploadedTime;
    }

    /**
     * Sets uploaded time.
     *
     * @param uploadedTime the uploaded time
     */
    public void setUploadedTime(LocalDateTime uploadedTime) {
        this.uploadedTime = uploadedTime;
    }

    @Override
    public String toString() {
        return "DBFileResponseDto{" +
                "uuid='" + uuid + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileType='" + fileType + '\'' +
                ", fileData=" + Arrays.toString(fileData) +
                ", uploadedTime=" + uploadedTime +
                '}';
    }
}
