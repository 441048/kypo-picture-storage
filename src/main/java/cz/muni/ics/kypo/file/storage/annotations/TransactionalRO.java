package cz.muni.ics.kypo.file.storage.annotations;

import org.springframework.core.annotation.AliasFor;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.*;

/**
 * Extending of the class {@link Transactional} which has <i>read-only</i> set to true.
 *
 * @author Pavel Seda
 */
@Transactional(rollbackFor = Exception.class, readOnly = true)
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface TransactionalRO {

    /**
     * Alias for <strong>transactionManager()<strong/>.
     *
     * @return the string
     */
    @AliasFor("transactionManager")
    String value() default "";

    /**
     * A <i>qualifier<i/> value for the specified transaction.
     *
     * @return the string
     */
    @AliasFor("value")
    String transactionManager() default "";

    /**
     * The transaction propagation type.
     *
     * @return the propagation
     */
    Propagation propagation() default Propagation.REQUIRED;

    /**
     * The transaction isolation type.
     *
     * @return the isolation
     */
    Isolation isolation() default Isolation.DEFAULT;

    /**
     * The timeout for this transaction (in seconds).
     *
     * @return the int
     */
    int timeout() default -1;

}
