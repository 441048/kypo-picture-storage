package cz.muni.ics.kypo.file.storage.api;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * The type Uploaded file response dto.
 *
 * @author Pavel Seda
 */
public class UploadedFileResponseDto {

    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private BigDecimal size;
    private LocalDateTime uploadedTime;

    /**
     * Instantiates a new Uploaded file response dto.
     */
    public UploadedFileResponseDto() {
    }

    /**
     * Instantiates a new Uploaded file response dto.
     *
     * @param fileName        the file name
     * @param fileDownloadUri the file download uri
     * @param fileType        the file type
     * @param size            the size in MB
     * @param uploadedTime    the uploaded time
     */
    public UploadedFileResponseDto(String fileName, String fileDownloadUri, String fileType, BigDecimal size, LocalDateTime uploadedTime) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
        this.uploadedTime = uploadedTime;
    }

    /**
     * Gets file name.
     *
     * @return the file name
     */
    @ApiModelProperty(value = "A name of a given file", example = "KYPO-intro-presentation.pptx")
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets file name.
     *
     * @param fileName the file name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets file download uri.
     *
     * @return the file download uri
     */
    @ApiModelProperty(value = "A type of a file", example = "https://localhost:8086/kypo-rest-file-storage/api/v1/download-file/56e8a7fb-64f4-43fb-af02-fe6b01691c76")
    public String getFileDownloadUri() {
        return fileDownloadUri;
    }

    /**
     * Sets file download uri.
     *
     * @param fileDownloadUri the file download uri
     */
    public void setFileDownloadUri(String fileDownloadUri) {
        this.fileDownloadUri = fileDownloadUri;
    }

    /**
     * Gets file type.
     *
     * @return the file type
     */
    @ApiModelProperty(value = "A type of a file", example = "application/vnd.openxmlformats-officedocument.presentationml.presentation")
    public String getFileType() {
        return fileType;
    }

    /**
     * Sets file type.
     *
     * @param fileType the file type
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * Gets size.
     *
     * @return the size
     */
    @ApiModelProperty(value = "A size of a given file in MB (megabytes)", example = "0.26706027984619140625")
    public BigDecimal getSize() {
        return size;
    }

    /**
     * Sets size.
     *
     * @param size the size
     */
    public void setSize(BigDecimal size) {
        this.size = size;
    }

    /**
     * Gets uploaded time.
     *
     * @return the uploaded time
     */
    @ApiModelProperty(value = "A time when a file was uploaded", example = "2019-07-09 14:50:09.467725")
    public LocalDateTime getUploadedTime() {
        return uploadedTime;
    }

    /**
     * Sets uploaded time.
     *
     * @param uploadedTime the uploaded time
     */
    public void setUploadedTime(LocalDateTime uploadedTime) {
        this.uploadedTime = uploadedTime;
    }

    @Override
    public String toString() {
        return "UploadedFileResponseDto{" +
                "fileName='" + fileName + '\'' +
                ", fileDownloadUri='" + fileDownloadUri + '\'' +
                ", fileType='" + fileType + '\'' +
                ", size=" + size +
                ", uploadedTime=" + uploadedTime +
                '}';
    }
}
