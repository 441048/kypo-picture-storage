package cz.muni.ics.kypo.file.storage.persistence.mapper;

import cz.muni.ics.kypo.file.storage.api.DBFileDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * The type Db file dto mapper.
 *
 * @author Pavel Seda
 */
public class DBFileDtoMapper implements RowMapper<DBFileDto> {

    @Override
    public DBFileDto mapRow(ResultSet resultSet, int i) throws SQLException {
        DBFileDto dbFileDto = new DBFileDto();
        dbFileDto.setUuid(resultSet.getObject("id", UUID.class).toString());
        dbFileDto.setFilePathOnDisk(resultSet.getString("file_path_on_disk"));
        dbFileDto.setFileName(resultSet.getString("file_name"));
        dbFileDto.setFileContentType(resultSet.getString("file_type"));
        dbFileDto.setUploadedTime(resultSet.getObject("uploaded_time", LocalDateTime.class));
        dbFileDto.setFileSize(resultSet.getBigDecimal("file_size"));
        return dbFileDto;
    }
}
