package cz.muni.ics.kypo.file.storage.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type My file not found exception.
 *
 * @author Pavel Seda
 */
@ResponseStatus(HttpStatus.INSUFFICIENT_STORAGE)
public class StorageSizeExceededException extends RuntimeException {

    /**
     * Instantiates a new Storage size exceeded exception.
     */
    public StorageSizeExceededException() {
    }

    /**
     * Instantiates a new Storage size exceeded exception.
     *
     * @param message the message
     */
    public StorageSizeExceededException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Storage size exceeded exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public StorageSizeExceededException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Storage size exceeded exception.
     *
     * @param cause the cause
     */
    public StorageSizeExceededException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Storage size exceeded exception.
     *
     * @param message            the message
     * @param cause              the cause
     * @param enableSuppression  the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    public StorageSizeExceededException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
