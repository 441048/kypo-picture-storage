package cz.muni.ics.kypo.file.storage.service.impl;

import cz.muni.ics.kypo.file.storage.exceptions.StorageSizeExceededException;
import cz.muni.ics.kypo.file.storage.persistence.impl.FileRepository;
import cz.muni.ics.kypo.file.storage.service.enums.Units;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * That class is used for checking if the file storage does not overreach a set size.
 *
 * @author Pavel Seda
 */
@Service
public class StorageSizeCheckerService {

    private Pattern digitDoublePattern = Pattern.compile("(\\d+(?:\\.\\d+)?)");
    @Value("${storage.max.size}")
    private String storageMaxSize;

    private FileRepository fileRepository;

    /**
     * Instantiates a new Storage size checker service.
     *
     * @param fileRepository the file repository
     */
    @Autowired
    public StorageSizeCheckerService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    /**
     * Check storage size.
     *
     * @param file the file
     */
    public void checkStorageSize(MultipartFile file) {
        if (storageMaxSize.contains(Units.KB.getName())) {
            Matcher matcher = digitDoublePattern.matcher(storageMaxSize);
            if (matcher.find()) {
                BigDecimal numberOfBytes = BigDecimal.valueOf(Double.parseDouble(matcher.group(1))).multiply(BigDecimal.valueOf(Units.KB.getNumberOfBytes()));
                checkSize(file, numberOfBytes);
            }
        }
        if (storageMaxSize.contains(Units.MB.getName())) {
            Matcher matcher = digitDoublePattern.matcher(storageMaxSize);
            if (matcher.find()) {
                BigDecimal numberOfBytes = BigDecimal.valueOf(Double.parseDouble(matcher.group(1))).multiply(BigDecimal.valueOf(Units.MB.getNumberOfBytes()));
                checkSize(file, numberOfBytes);
            }
        }
        if (storageMaxSize.contains(Units.GB.getName())) {
            Matcher matcher = digitDoublePattern.matcher(storageMaxSize);
            if (matcher.find()) {
                BigDecimal numberOfBytes = BigDecimal.valueOf(Double.parseDouble(matcher.group(1))).multiply(BigDecimal.valueOf(Units.GB.getNumberOfBytes()));
                checkSize(file, numberOfBytes);
            }
        }
    }

    /**
     * Comparison of two BigDecimal objects: http://resolvethis.com/compare-two-bigdecimals-java/
     *
     * @param file
     * @param intendedMaxStorageSize
     */
    private void checkSize(MultipartFile file, BigDecimal intendedMaxStorageSize) {
        BigDecimal actualStorageSizePlusGivenFile = BigDecimal.valueOf(file.getSize() / Units.MB.getNumberOfBytes()).add(fileRepository.getActualStorageSize());
        if (intendedMaxStorageSize.compareTo(actualStorageSizePlusGivenFile) < 0)
            throw new StorageSizeExceededException("Storage size exceeded.");
    }

}
