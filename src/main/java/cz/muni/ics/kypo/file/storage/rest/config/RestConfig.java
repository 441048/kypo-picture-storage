package cz.muni.ics.kypo.file.storage.rest.config;

import cz.muni.ics.kypo.commons.swagger.SwaggerConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * The type Rest config.
 *
 * @author Pavel Seda
 */
@Configuration
@Import({WebConfigRestFileStorageService.class, SwaggerConfig.class})
public class RestConfig {
}
