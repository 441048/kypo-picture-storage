package cz.muni.ics.kypo.file.storage.startupconstructs;

import cz.muni.ics.kypo.file.storage.exceptions.FileNotCreatedException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * The type Start up runners.
 *
 * @author Pavel Seda
 */
@Component
public class StartUpRunners {

    @Value("${path.to.file.storage.on.disk}")
    private String directoryForFiles;

    /**
     * Create directories if not exists.
     */
    @PostConstruct
    public void createDirectoriesIfNotExists() {
        try {
            Files.createDirectories(Paths.get(directoryForFiles));
        } catch (IOException e) {
            throw new FileNotCreatedException("It is not possible to create directories in a given folder. Please check app privileges to create folder for storing files in your system.", e);
        }
    }
}
