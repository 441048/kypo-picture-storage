package cz.muni.ics.kypo.file.storage.aop;

import org.aspectj.lang.annotation.Pointcut;

/**
 * The type Common join point config.
 *
 * @author Pavel Seda
 * @author Dominik Pilar
 */
public class CommonJoinPointConfig {

    /**
     * Data layer execution logging debug.
     */
    @Pointcut("execution(* cz.muni.ics.kypo.file.storage.persistence.impl*.*(..))")
    public void dataLayerExecutionLoggingDebug() {
    }

    /**
     * Service layer execution logging debug.
     */
    @Pointcut("execution(* cz.muni.ics.kypo.file.storage.service.impl.*.*(..))")
    public void serviceLayerExecutionLoggingDebug() {
    }

    /**
     * Startup constructs execution logging info.
     */
    @Pointcut("execution(* cz.muni.ics.kypo.file.storage.startupconstructs.*.*(..))")
    public void startupConstructsExecutionLoggingInfo() {
    }

    /**
     * Rest layer execution logging debug.
     */
    @Pointcut("execution(* cz.muni.ics.kypo.file.storage.rest.*.*(..))")
    public void restLayerExecutionLoggingDebug() {
    }

    /**
     * Rest layer execution logging error.
     */
    @Pointcut("execution(* cz.muni.ics.kypo.file.storage.rest.CustomRestExceptionHandler.*(..))")
    public void restLayerExecutionLoggingError() {
    }
}
