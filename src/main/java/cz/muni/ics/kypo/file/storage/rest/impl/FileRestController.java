package cz.muni.ics.kypo.file.storage.rest.impl;

import cz.muni.ics.kypo.file.storage.api.DBFileDto;
import cz.muni.ics.kypo.file.storage.api.DBFileResponseDto;
import cz.muni.ics.kypo.file.storage.api.UploadedFileResponseDto;
import cz.muni.ics.kypo.file.storage.service.impl.FileStorageService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Rest controller for the file storage service.
 *
 * @author Pavel Seda
 */
@Api(value = "Api resources for file storage.")
@RequestMapping("/")
@RestController
public class FileRestController {

    private FileStorageService fileStorageService;

    /**
     * Instantiates a new File rest controller.
     *
     * @param fileStorageService the file storage service
     */
    @Autowired
    public FileRestController(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    /**
     * Upload file response entity.
     *
     * @param file the file to store
     * @return the response entity containing data about uploaded file, e.g., the URL to download the file
     */
    @ApiOperation(httpMethod = "POST",
            value = "Upload a new file.",
            response = UploadedFileResponseDto.class,
            responseContainer = "List",
            nickname = "uploadFile",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Given file saved.", response = UploadedFileResponseDto.class),
    })
    @PostMapping(path = "upload-file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UploadedFileResponseDto> uploadFile(
            @ApiParam(value = "A given file to store.", required = true)
            @RequestParam(value = "file", required = true) MultipartFile file) {
        DBFileDto dbFile = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download-file/")
                .path(dbFile.getUuid())
                .toUriString();

        return ResponseEntity.ok(new UploadedFileResponseDto(dbFile.getFileName(), fileDownloadUri,
                dbFile.getFileContentType(), dbFile.getFileSize(), dbFile.getUploadedTime()));
    }

    /**
     * Upload multiple files.
     *
     * @param files the files
     * @return the list of files including the data, e.g., where to download them
     */
    @ApiOperation(httpMethod = "POST",
            value = "Upload a new files.",
            response = UploadedFileResponseDto.class,
            nickname = "uploadMultipleFiles",
            responseContainer = "List",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Given files saved.", response = UploadedFileResponseDto.class, responseContainer = "List"),
    })
    @PostMapping(path = "upload-multiple-files", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UploadedFileResponseDto> uploadMultipleFiles(
            @ApiParam(value = "A given files to store.", required = true)
            @RequestParam(value = "files", required = true) MultipartFile[] files) {
        return List.of(files)
                .stream()
                .map(file -> uploadFile(file).getBody())
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Download file (image, text file etc.) based on a given uuid identifier.
     *
     * @param fileId the file id (uuid identifier to identify the specific file)
     * @return the response entity containing selected file based on uuid
     */
    @ApiOperation(httpMethod = "POST",
            value = "Download a file.",
            response = Resource.class,
            nickname = "downloadFile"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Given files saved.", response = Resource.class),
    })
    @GetMapping(path = "download-file/{fileId}", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Resource> downloadFile(
            @ApiParam(value = "An identifier of a file.", required = true)
            @PathVariable String fileId) {
        DBFileResponseDto dbFile = fileStorageService.getFile(fileId);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getFileData()));
    }

}
