package cz.muni.ics.kypo.file.storage.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type My file not found exception.
 *
 * @author Pavel Seda
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class FileNotFoundException extends RuntimeException {

    /**
     * Instantiates a new My file not found exception.
     *
     * @param message the message
     */
    public FileNotFoundException(String message) {
        super(message);
    }

    /**
     * Instantiates a new File not found exception.
     */
    public FileNotFoundException() {
    }

    /**
     * Instantiates a new My file not found exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public FileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new File not found exception.
     *
     * @param cause the cause
     */
    public FileNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new File not found exception.
     *
     * @param message            the message
     * @param cause              the cause
     * @param enableSuppression  the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    public FileNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
