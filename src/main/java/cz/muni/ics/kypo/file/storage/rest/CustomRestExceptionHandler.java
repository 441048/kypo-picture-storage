package cz.muni.ics.kypo.file.storage.rest;

import cz.muni.ics.kypo.file.storage.exceptions.FileNotFoundException;
import cz.muni.ics.kypo.file.storage.exceptions.FileNotStoredException;
import cz.muni.ics.kypo.file.storage.exceptions.InvalidCharactersException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * That is the base implementation of @ControllerAdvice, which is used so that all the handlers can
 * be managed from a central location. Another approach is to associate one handler with a set of
 * Controllers See:
 * https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/bind/annotation/ControllerAdvice.html
 * </p>
 *
 * @author Pavel Šeda
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * The constant URL_PATH_HELPER.
     */
    protected static final UrlPathHelper URL_PATH_HELPER = new UrlPathHelper();

    private Exception getInitialException(Exception exception) {
        while (exception.getCause() != null) {
            exception = (Exception) exception.getCause();
        }
        return exception;
    }

    /**
     * Handle file not found exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @param req     the req
     * @return the response entity
     */
    @ExceptionHandler(FileNotFoundException.class)
    public ResponseEntity<Object> handleFileNotFoundException(final FileNotFoundException ex, final WebRequest request,
                                                              HttpServletRequest req) {
        final ApiError apiError =
                new ApiError.ApiErrorBuilder(FileNotFoundException.class.getAnnotation(ResponseStatus.class).value(), getInitialException(ex).getLocalizedMessage())
                        .setError(FileNotFoundException.class.getAnnotation(ResponseStatus.class).reason()).setPath(URL_PATH_HELPER.getRequestUri(req)).build();
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    /**
     * Handle file not stored exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @param req     the req
     * @return the response entity
     */
    @ExceptionHandler(FileNotStoredException.class)
    public ResponseEntity<Object> handleFileNotStoredException(final FileNotStoredException ex, final WebRequest request,
                                                               HttpServletRequest req) {
        final ApiError apiError =
                new ApiError.ApiErrorBuilder(FileNotStoredException.class.getAnnotation(ResponseStatus.class).value(), getInitialException(ex).getLocalizedMessage())
                        .setError(FileNotStoredException.class.getAnnotation(ResponseStatus.class).reason()).setPath(URL_PATH_HELPER.getRequestUri(req)).build();
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    /**
     * Handle invalid characters exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @param req     the req
     * @return the response entity
     */
    @ExceptionHandler(InvalidCharactersException.class)
    public ResponseEntity<Object> handleInvalidCharactersException(final InvalidCharactersException ex, final WebRequest request,
                                                                   HttpServletRequest req) {
        final ApiError apiError =
                new ApiError.ApiErrorBuilder(InvalidCharactersException.class.getAnnotation(ResponseStatus.class).value(), getInitialException(ex).getLocalizedMessage())
                        .setError(InvalidCharactersException.class.getAnnotation(ResponseStatus.class).reason()).setPath(URL_PATH_HELPER.getRequestUri(req)).build();
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    /**
     * Handle all response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @param req     the req
     * @return the response entity
     */
    // 500
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request, HttpServletRequest req) {

        String err = "Some error occurred. Please try again or contact administrator.";

        final ApiError apiError = new ApiError.ApiErrorBuilder(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage())
                .setError(err).setPath(URL_PATH_HELPER.getRequestUri(req)).build();
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

}
