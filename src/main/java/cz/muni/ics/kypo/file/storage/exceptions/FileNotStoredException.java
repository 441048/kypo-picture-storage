package cz.muni.ics.kypo.file.storage.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type file not stored exception.
 *
 * @author Pavel Seda
 */
@ResponseStatus(HttpStatus.NOT_MODIFIED)
public class FileNotStoredException extends RuntimeException {

    /**
     * Instantiates a new File not stored exception.
     */
    public FileNotStoredException() {
    }

    /**
     * Instantiates a new File not stored exception.
     *
     * @param message the message
     */
    public FileNotStoredException(String message) {
        super(message);
    }

    /**
     * Instantiates a new File not stored exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public FileNotStoredException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new File not stored exception.
     *
     * @param cause the cause
     */
    public FileNotStoredException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new File not stored exception.
     *
     * @param message            the message
     * @param cause              the cause
     * @param enableSuppression  the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    public FileNotStoredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
