package cz.muni.ics.kypo.file.storage.api;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * The type Db file dto.
 *
 * @author Pavel Seda
 */
public class DBFileDto {

    private String uuid;
    private String fileName;
    private String fileContentType;
    private String filePathOnDisk;
    private LocalDateTime uploadedTime;
    private BigDecimal fileSize;

    /**
     * Instantiates a new Db file dto.
     */
    public DBFileDto() {
    }

    /**
     * Instantiates a new Db file dto.
     *
     * @param uuid            the uuid
     * @param fileName        the file name
     * @param fileContentType the file content type
     * @param filePathOnDisk  the file path on disk
     * @param uploadedTime    the uploaded time
     * @param fileSize        the file size
     */
    public DBFileDto(String uuid, String fileName, String fileContentType, String filePathOnDisk, LocalDateTime uploadedTime, BigDecimal fileSize) {
        this.uuid = uuid;
        this.fileName = fileName;
        this.fileContentType = fileContentType;
        this.filePathOnDisk = filePathOnDisk;
        this.uploadedTime = uploadedTime;
        this.fileSize = fileSize;
    }

    /**
     * Gets uuid.
     *
     * @return the uuid
     */

    /**
     * Gets full name.
     *
     * @return the full name
     */
    @ApiModelProperty(value = "Unique reference id to the file", example = "56e8a7fb-64f4-43fb-af02-fe6b01691c76")
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets uuid.
     *
     * @param uuid the uuid
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Gets file name.
     *
     * @return the file name
     */
    @ApiModelProperty(value = "A name of a given file", example = "KYPO-intro-presentation.pptx")
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets file name.
     *
     * @param fileName the file name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets file type.
     *
     * @return the file type
     */
    @ApiModelProperty(value = "A type of a file", example = "application/vnd.openxmlformats-officedocument.presentationml.presentation")
    public String getFileContentType() {
        return fileContentType;
    }

    /**
     * Sets file type.
     *
     * @param fileContentType the file type
     */
    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    /**
     * Gets file path on disk.
     *
     * @return the file path on disk
     */
    @ApiModelProperty(value = "A path where a files are stored in the file system", example = "/home/seda/file-storage-data/27922eb3-f5d2-4471-816b-5506764f1411")
    public String getFilePathOnDisk() {
        return filePathOnDisk;
    }

    /**
     * Sets file path on disk.
     *
     * @param filePathOnDisk the file path on disk
     */
    public void setFilePathOnDisk(String filePathOnDisk) {
        this.filePathOnDisk = filePathOnDisk;
    }

    /**
     * Gets uploaded time.
     *
     * @return the uploaded time
     */
    @ApiModelProperty(value = "A time when a file was uploaded", example = "2019-07-09 14:50:09.467725")
    public LocalDateTime getUploadedTime() {
        return uploadedTime;
    }

    /**
     * Sets uploaded time.
     *
     * @param uploadedTime the uploaded time
     */
    public void setUploadedTime(LocalDateTime uploadedTime) {
        this.uploadedTime = uploadedTime;
    }

    /**
     * Gets file size.
     *
     * @return the file size
     */
    @ApiModelProperty(value = "A size of a given file in MB (megabytes)", example = "0.26706027984619140625")
    public BigDecimal getFileSize() {
        return fileSize;
    }

    /**
     * Sets file size.
     *
     * @param fileSize the file size
     */
    public void setFileSize(BigDecimal fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DBFileDto)) return false;
        DBFileDto dbFileDto = (DBFileDto) o;
        return getFileSize() == dbFileDto.getFileSize() &&
                Objects.equals(getUuid(), dbFileDto.getUuid()) &&
                Objects.equals(getFileName(), dbFileDto.getFileName()) &&
                Objects.equals(getFileContentType(), dbFileDto.getFileContentType()) &&
                Objects.equals(getFilePathOnDisk(), dbFileDto.getFilePathOnDisk()) &&
                Objects.equals(getUploadedTime(), dbFileDto.getUploadedTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUuid(), getFileName(), getFileContentType(), getFilePathOnDisk(), getUploadedTime(), getFileSize());
    }

    @Override
    public String toString() {
        return "DBFileDto{" +
                "uuid='" + uuid + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileContentType='" + fileContentType + '\'' +
                ", filePathOnDisk='" + filePathOnDisk + '\'' +
                ", uploadedTime=" + uploadedTime +
                ", fileSize=" + fileSize +
                '}';
    }
}
