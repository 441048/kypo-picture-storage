package cz.muni.ics.kypo.file.storage.service.enums;

/**
 * The enum Units.
 *
 * @author Pavel Seda
 */
public enum Units {
    /**
     * B units.
     */
    B("B", "bytes", 1),
    /**
     * Kb units.
     */
    KB("KB", "kilobytes", 1024),
    /**
     * Mb units.
     */
    MB("MB", "megabytes", 1_048_576),
    /**
     * Gb units.
     */
    GB("GB", "gigabytes", 1_073_741_824);

    private String name;
    private String description;
    private long numberOfBytes;

    private Units(String name, String description, long numberOfBytes) {
        this.name = name;
        this.description = description;
        this.numberOfBytes = numberOfBytes;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets number of bytes.
     *
     * @return the number of bytes
     */
    public long getNumberOfBytes() {
        return numberOfBytes;
    }

    /**
     * Sets number of bytes.
     *
     * @param numberOfBytes the number of bytes
     */
    public void setNumberOfBytes(long numberOfBytes) {
        this.numberOfBytes = numberOfBytes;
    }
}
