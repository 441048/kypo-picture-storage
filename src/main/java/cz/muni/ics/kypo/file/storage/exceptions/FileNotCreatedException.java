package cz.muni.ics.kypo.file.storage.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type File not created exception.
 *
 * @author Pavel Seda
 */
@ResponseStatus(HttpStatus.NOT_MODIFIED)
public class FileNotCreatedException extends RuntimeException {

    /**
     * Instantiates a new File not created exception.
     */
    public FileNotCreatedException() {
    }

    /**
     * Instantiates a new File not created exception.
     *
     * @param message the message
     */
    public FileNotCreatedException(String message) {
        super(message);
    }

    /**
     * Instantiates a new File not created exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public FileNotCreatedException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new File not created exception.
     *
     * @param cause the cause
     */
    public FileNotCreatedException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new File not created exception.
     *
     * @param message            the message
     * @param cause              the cause
     * @param enableSuppression  the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    public FileNotCreatedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
