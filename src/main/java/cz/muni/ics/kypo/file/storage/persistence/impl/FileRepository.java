package cz.muni.ics.kypo.file.storage.persistence.impl;

import cz.muni.ics.kypo.file.storage.api.DBFileDto;
import cz.muni.ics.kypo.file.storage.persistence.mapper.DBFileDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

/**
 * The type File repository.
 *
 * @author Pavel Seda
 */
@Repository
public class FileRepository {

    private JdbcTemplate jdbcTemplate;

    /**
     * Instantiates a new File repository.
     *
     * @param jdbcTemplate the jdbc template
     */
    @Autowired
    public FileRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Find a file by id.
     *
     * @param fileId the file id
     * @return the file
     */
    public Optional<DBFileDto> findById(String fileId) {
        return Optional.ofNullable(jdbcTemplate.queryForObject("SELECT id, file_path_on_disk, file_name, file_type, uploaded_time, file_size FROM file_storage WHERE id = ?",
                new Object[]{UUID.fromString(fileId)},
                new DBFileDtoMapper()));
    }

    /**
     * Save a given file.
     *
     * @param dbFileDto the db file dto
     * @return the db file dto
     */
    public DBFileDto save(DBFileDto dbFileDto) {
        jdbcTemplate.update("INSERT INTO file_storage (id, file_path_on_disk, file_name, file_type, uploaded_time, file_size) VALUES (?, ?, ?, ?, ?, ?)",
                new Object[]{UUID.fromString(dbFileDto.getUuid()), dbFileDto.getFilePathOnDisk(), dbFileDto.getFileName(), dbFileDto.getFileContentType(), dbFileDto.getUploadedTime(), dbFileDto.getFileSize()});
        return dbFileDto;
    }

    /**
     * Update actual storage size.
     *
     * @param fileSize the file size
     */
    public void updateActualStorageSize(BigDecimal fileSize) {
        jdbcTemplate.update("UPDATE actual_storage_size SET storage_size = storage_size + ?", fileSize);
    }

    /**
     * Gets actual storage size.
     *
     * @return the actual storage size
     */
    public BigDecimal getActualStorageSize() {
        return jdbcTemplate.queryForObject("SELECT storage_size FROM actual_storage_size", BigDecimal.class);
    }
}
