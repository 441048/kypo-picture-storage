package cz.muni.ics.kypo.file.storage.service.impl;

import cz.muni.ics.kypo.file.storage.api.DBFileDto;
import cz.muni.ics.kypo.file.storage.api.DBFileResponseDto;
import cz.muni.ics.kypo.file.storage.exceptions.FileNotStoredException;
import cz.muni.ics.kypo.file.storage.exceptions.FileNotFoundException;
import cz.muni.ics.kypo.file.storage.exceptions.InvalidCharactersException;
import cz.muni.ics.kypo.file.storage.exceptions.StorageSizeExceededException;
import cz.muni.ics.kypo.file.storage.persistence.impl.FileRepository;
import cz.muni.ics.kypo.file.storage.service.enums.Units;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * The type File storage service.
 *
 * @author Pavel Seda
 */
@Service
@Transactional
public class FileStorageService {

    @Value("${path.to.file.storage.on.disk}")
    private String directoryForFiles;

    private FileRepository fileRepository;
    private StorageSizeCheckerService storageSizeCheckerService;

    /**
     * Instantiates a new File storage service.
     *
     * @param fileRepository            the file repository
     * @param storageSizeCheckerService the storage size checker service
     */
    @Autowired
    public FileStorageService(FileRepository fileRepository, StorageSizeCheckerService storageSizeCheckerService) {
        this.fileRepository = fileRepository;
        this.storageSizeCheckerService = storageSizeCheckerService;
    }

    /**
     * Store file db file dto.
     *
     * @param file the file
     * @return the db file dto
     */
    public DBFileDto storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new InvalidCharactersException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            storageSizeCheckerService.checkStorageSize(file);

            String generatedUUID = generateUniqueIdentifier();

            Path filePath = Paths.get(directoryForFiles + "/" + generatedUUID);
            writeFileDataOnDisk(filePath, file);

            DBFileDto dbFile = fileRepository.save(
                    new DBFileDto(
                            generatedUUID,
                            fileName,
                            file.getContentType(),
                            filePath.toAbsolutePath().toString(),
                            LocalDateTime.now(Clock.systemUTC()),
                            BigDecimal.valueOf(file.getSize()).divide(BigDecimal.valueOf(Units.MB.getNumberOfBytes()))));

            // store the file size in MB (megabytes)
            fileRepository.updateActualStorageSize(dbFile.getFileSize().divide(BigDecimal.valueOf(Units.MB.getNumberOfBytes())));
            return dbFile;
        } catch (IOException ex) {
            throw new FileNotStoredException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }


    private String generateUniqueIdentifier() {
        // here, we generate UUID by JVM instead of in DB
        return UUID.randomUUID().toString();
    }

    private void writeFileDataOnDisk(Path fileToWrite, MultipartFile file) throws IOException {
        if (!Files.exists(fileToWrite)) {
            Files.write(fileToWrite, file.getBytes(), StandardOpenOption.CREATE);
        } else {
            throw new FileNotStoredException("Could not store file. Please try it again!");
        }
    }

    /**
     * Gets file.
     *
     * @param fileId the file id
     * @return the file
     */
    public DBFileResponseDto getFile(String fileId) {
        DBFileDto dbFileDto = fileRepository.findById(fileId)
                .orElseThrow(() -> new FileNotFoundException("File with id " + fileId + " not found."));

        DBFileResponseDto dbFileResponseDto = new DBFileResponseDto();
        dbFileResponseDto.setFileName(dbFileDto.getFileName());
        dbFileResponseDto.setFileType(dbFileDto.getFileContentType());
        dbFileResponseDto.setFileData(getFileOnDisk(Paths.get(dbFileDto.getFilePathOnDisk())));
        dbFileResponseDto.setUploadedTime(dbFileDto.getUploadedTime());
        dbFileResponseDto.setUuid(dbFileDto.getUuid());
        return dbFileResponseDto;
    }

    private byte[] getFileOnDisk(Path fileOnDisk) {
        try {
            return Files.readAllBytes(fileOnDisk);
        } catch (IOException ex) {
            throw new FileNotFoundException("File with id " + fileOnDisk.getFileName().toString() + " not found.", ex);
        }
    }

    private long storageMaxSize(Path storageRootDirectory) {
        try {
            return Files.walk(storageRootDirectory)
                    .map(p -> p.toFile())
                    .filter(File::isFile)
                    .mapToLong(p -> p.length())
                    .sum();
        } catch (IOException e) {
            throw new StorageSizeExceededException("Storage size exceeded.", e);
        }
    }

}
