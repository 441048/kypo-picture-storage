package cz.muni.ics.kypo.file.storage.aop;

/**
 * The enum Logging levels.
 *
 * @author Pavel Seda
 */
public enum LoggingLevels {

    /**
     * The Debug.
     */
    DEBUG("DEBUG", "Logging level DEBUG"),
    /**
     * The Info.
     */
    INFO("INFO", "Logging level INFO"),
    /**
     * The Error.
     */
    ERROR("ERROR", "Logging level ERRROR");

    private String name;
    private String description;

    private LoggingLevels(String name, String description){
        this.name = name;
        this.description = description;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
