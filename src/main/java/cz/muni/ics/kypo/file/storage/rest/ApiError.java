package cz.muni.ics.kypo.file.storage.rest;

import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * Represents a possible representation of errors to be used with the @ControllerAdvice global
 * exceptions handler.
 * </p>
 *
 * @author Pavel Šeda
 */
public class ApiError {

    /**
     * The Timestamp.
     */
    protected long timestamp;
    /**
     * The Status.
     */
    protected HttpStatus status;
    /**
     * The Message.
     */
    protected String message;
    /**
     * The Errors.
     */
    protected List<String> errors;
    /**
     * The Path.
     */
    protected String path;

    /**
     * The type Api error builder.
     */
    public static class ApiErrorBuilder {
        // required member variables
        private final HttpStatus status;
        private final String message;
        // optional member variables
        private List<String> errors = new ArrayList<>();
        private String path = "";

        /**
         * Instantiates a new Api error builder.
         *
         * @param status  the status
         * @param message the message
         */
        public ApiErrorBuilder(HttpStatus status, String message) {
            Objects.requireNonNull(status, "HttpStatus is necessary to not be null.");
            Objects.requireNonNull(message, "It is required to provide error message.");
            this.status = status;
            this.message = message;
        }

        /**
         * Sets error.
         *
         * @param error the error
         * @return the error
         */
        public ApiErrorBuilder setError(String error) {
            Objects.requireNonNull(error, "Given error could not be null");
            this.errors = Arrays.asList(error);
            return this;
        }

        /**
         * Sets errors.
         *
         * @param errors the errors
         * @return the errors
         */
        public ApiErrorBuilder setErrors(List<String> errors) {
            Objects.requireNonNull(errors, "Given list of errors could not be null");
            this.errors = errors;
            return this;
        }

        /**
         * Sets path.
         *
         * @param path the path
         * @return the path
         */
        public ApiErrorBuilder setPath(String path) {
            Objects.requireNonNull(path, "Given path could not be null");
            this.path = path;
            return this;
        }

        /**
         * Build api error.
         *
         * @return the api error
         */
        public ApiError build() {
            return new ApiError(this);
        }
    }

    /**
     * Instantiates a new Api error.
     *
     * @param builder the builder
     */
    protected ApiError(ApiErrorBuilder builder) {
        super();
        this.timestamp = System.currentTimeMillis();
        this.status = builder.status;
        this.message = builder.message;
        this.errors = builder.errors;
        this.path = builder.path;
    }

    /**
     * Gets timestamp.
     *
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Sets timestamp.
     *
     * @param timestamp the timestamp
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public HttpStatus getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(final HttpStatus status) {
        this.status = status;
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     *
     * @param message the message
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * Gets errors.
     *
     * @return the errors
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     * Sets errors.
     *
     * @param errors the errors
     */
    public void setErrors(final List<String> errors) {
        this.errors = errors;
    }

    /**
     * Sets error.
     *
     * @param error the error
     */
    public void setError(final String error) {
        errors = Arrays.asList(error);
    }

    /**
     * Gets path.
     *
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * Sets path.
     *
     * @param path the path
     */
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "ApiError [timestamp=" + timestamp + ", status=" + status + ", message=" + message + ", errors=" + errors + ", path=" + path + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApiError)) return false;
        ApiError apiError = (ApiError) o;
        return getTimestamp() == apiError.getTimestamp() &&
                getStatus() == apiError.getStatus() &&
                Objects.equals(getMessage(), apiError.getMessage()) &&
                Objects.equals(getErrors(), apiError.getErrors()) &&
                Objects.equals(getPath(), apiError.getPath());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTimestamp(), getStatus(), getMessage(), getErrors(), getPath());
    }
}
