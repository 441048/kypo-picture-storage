package cz.muni.ics.kypo.file.storage;

import cz.muni.ics.kypo.file.storage.rest.config.RestConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * The type Kypo file storage application.
 *
 * @author Pavel Šeda
 */
@Import(RestConfig.class)
@PropertySource("file:${path.to.config.file}")
@SpringBootApplication
public class KypoFileStorageApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(KypoFileStorageApplication.class);
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(KypoFileStorageApplication.class, args);
    }

}
