package cz.muni.ics.kypo.file.storage.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type My file not found exception.
 *
 * @author Pavel Seda
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidCharactersException extends RuntimeException {

    /**
     * Instantiates a new Invalid characters exception.
     */
    public InvalidCharactersException() {
    }

    /**
     * Instantiates a new Invalid characters exception.
     *
     * @param message the message
     */
    public InvalidCharactersException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Invalid characters exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public InvalidCharactersException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Invalid characters exception.
     *
     * @param cause the cause
     */
    public InvalidCharactersException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Invalid characters exception.
     *
     * @param message            the message
     * @param cause              the cause
     * @param enableSuppression  the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    public InvalidCharactersException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
