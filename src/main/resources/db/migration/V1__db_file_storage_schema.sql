-- CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
--    id uuid DEFAULT uuid_generate_v4 (),

CREATE TABLE file_storage (
    id uuid NOT NULL,
    file_path_on_disk varchar(255) NOT NULL,
    file_name varchar(255) NOT NULL,
    file_type varchar(255) NOT NULL,
    uploaded_time timestamp NOT NULL,
    file_size decimal NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE actual_storage_size (
    id bigserial NOT NULL,
    storage_size decimal NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO actual_storage_size (id, storage_size) VALUES (nextval('actual_storage_size_id_seq'), 0);
